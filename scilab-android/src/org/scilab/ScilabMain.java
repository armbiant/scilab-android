package org.scilab;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class ScilabMain extends Activity implements OnClickListener,
		OnEditorActionListener {
	private TextView console;
	private EditText command;
	private Button execute;

	public native void setEnvironment(String key, String value);

	public native int run();

	public native void sendCommand(String command);

	public native String readResult();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		final int exit;

		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.scilab_main);

		/*
		 * User interface elements. Pretty basic for now.
		 */
		this.console = (TextView) this.findViewById(R.id.term_output);
		this.command = (EditText) this.findViewById(R.id.term_entry);
		this.execute = (Button) this.findViewById(R.id.term_entry_send);

		/*
		 * Send the command when the button is clicked or when Enter is pressed.
		 */
		this.execute.setOnClickListener(this);
		this.command.setOnEditorActionListener(this);

		/*
		 * Launch Scilab attached to our emulated terminal.
		 */
		// exit = this.run();

		// Toast.makeText(this, "Exit status = " + exit, Toast.LENGTH_SHORT)
		// .show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getMenuInflater().inflate(R.menu.scilab_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_clear_console) {
			this.clearConsole();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void copyAssets() throws IOException {
		final AssetManager assets;
		final File files;
		final FileOutputStream lock;

		assets = this.getAssets();
		files = this.getFilesDir();
		lock = this.openFileOutput(files.getAbsolutePath() + File.separator
				+ "lock", MODE_PRIVATE);

		lock.write("scilab lock for assets".getBytes());
		lock.close();

		this.copyAssets(assets, files, ".");
	}

	private void copyAssets(AssetManager assets, File destination,
			String directory) throws IOException {
		File file;

		for (String path : assets.list(directory)) {
			file = new File(path);

			if (!file.isDirectory()) {
				Utils.copyFile(file, new File(destination.getAbsolutePath()
						+ File.separator + file.getName()));
			} else {
				new File(destination.getAbsolutePath() + File.separator
						+ file.getName()).mkdirs();
				this.copyAssets(assets, destination, file.getName());
			}
		}
	}

	private void clearConsole() {
		if (this.console.getEditableText() != null) {
			this.console.getEditableText().clear();
		}
	}

	private void commit() {
		final String command;

		/*
		 * Nothing in the buffer, nothing to send.
		 */
		if (this.command.getText().length() == 0) {
			return;
		}

		command = this.command.getText().toString();

		/*
		 * Clear the `console'.
		 */
		if (command.equals("clear") || command.equals("reset")) {
			this.clearConsole();
		} else {
			/*
			 * FIXME: just echo the command for now.
			 */
			this.console.append(this.command.getText().toString() + "\n");
		}

		this.command.getText().clear();
	}

	public void onClick(View v) {
		if (v.getId() == R.id.term_entry_send) {
			this.commit();
		}
	}

	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (v.getId() == R.id.term_entry) {
			/*
			 * The enter key has been pressed.
			 */
			if (event != null) {
				this.commit();
			}

			return true;
		}

		return false;
	}
}
