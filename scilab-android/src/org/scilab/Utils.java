package org.scilab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public final class Utils {
	public static void copyFile(File sourceFile, File destinationFile)
			throws IOException {
		FileChannel source, destination;
		long count, size;

		source = null;
		destination = null;

		if (!destinationFile.exists()) {
			destinationFile.createNewFile();
		}

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destinationFile).getChannel();

			count = 0;
			size = source.size();

			while ((count += destination.transferFrom(source, count, size
					- count)) < size) {
				;
			}

		} finally {
			if (source != null) {
				source.close();
			}

			if (destination != null) {
				destination.close();
			}
		}
	}
}
