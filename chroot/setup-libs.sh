#!/bin/bash

# Author:  Guillaume Mazoyer <respawneral@gmail.com>
# License: see Scilab license
#
# Based on:
#   http://svn.wildfiregames.com/public/ps/trunk/build/android/setup-libs.sh

set -e
set -o nounset

# If `true' all `build_*' variables (except the one for hdf5) need to be set to
# `true' too.
reset_prefix=true

build_ncurses=true
build_readline=true
build_libxml2=true
build_pcre=true
build_gettext=true
build_blas=true
build_lapack=true
build_hdf5=true

PREFIX="/data/local/usr"
HOST="arm-linux-gnueabi"
JOBS=${JOBS:="-j2"}

check_and_install() {
  packages=("gcc-arm-linux-gnueabi" "g++-arm-linux-gnueabi" \
            "gfortran-arm-linux-gnueabi" "binutils-arm-linux-gnueabi" \
            "pkg-config-arm-linux-gnueabi" "gcc" "gfortran" "g++" \
            "build-essential" "chrpath" "wget")

  if [ ${EUID} -ne 0 ]; then
    echo "Go root to run this script."
    echo "You're doing this in a chroot right?" 
    exit 1
  fi

  for package in ${packages[@]}; do
    installed=$(dpkg-query -W ${package})

    if [ "${installed}" = "1" ]; then
      apt-get install -q=2 ${package}
    fi
  done
}

# Install needed packages
check_and_install

# Prepare an `Android like' directory to install dependencies
if [ "${reset_prefix}" = "true" ]; then
  # Remove all previous files to be sure we are strating from scratch.
  rm -rf ${PREFIX}
fi
mkdir -p ${PREFIX}/lib

# Download source code of each dependency
mkdir -p files
pushd files

if [ ! -e ncurses-5.9.tar.gz ]; then
  wget ftp://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz
fi

if [ ! -e readline-6.2.tar.gz ]; then
  wget ftp://ftp.gnu.org/pub/gnu/readline/readline-6.2.tar.gz
fi

if [ ! -e libxml2-2.7.8.tar.gz ]; then
  wget ftp://xmlsoft.org/libxml2/libxml2-2.7.8.tar.gz
fi

if [ ! -e pcre-8.30.tar.gz ]; then
  wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.30.tar.gz
fi

if [ ! -e gettext-0.18.1.1.tar.gz ]; then
  wget http://ftp.gnu.org/pub/gnu/gettext/gettext-0.18.1.1.tar.gz
fi

if [ ! -e blas.tgz  ]; then
  wget http://www.netlib.org/blas/blas.tgz
fi

if [ ! -e lapack-3.4.1.tgz ]; then
  wget http://www.netlib.org/lapack/lapack-3.4.1.tgz
fi

if [ ! -e hdf5-1.8.9.tar.gz ]; then
  wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.9.tar.gz
fi

popd

# Do the dirty job aka "go back to work!" ... "nope, compiling ;)"
mkdir -p temp

if [ "${build_ncurses}" = "true" ]; then
  rm -rf temp/ncurses-5.9
  tar xzf files/ncurses-5.9.tar.gz -C temp
  pushd temp/ncurses-5.9
  ./configure --host=${HOST} --prefix=${PREFIX} --with-shared
  make ${JOBS}
  make install
  popd
fi

if [ "${build_readline}" = "true" ]; then
  rm -rf temp/readline-6.2
  tar xzf files/readline-6.2.tar.gz -C temp
  pushd temp/readline-6.2
  ./configure --host=${HOST} --prefix=${PREFIX} --with-curses
  make ${JOBS}
  make install
  popd
fi

if [ "${build_libxml2}" = "true" ]; then
  rm -rf temp/libxml2-2.7.8
  tar xzf files/libxml2-2.7.8.tar.gz -C temp
  pushd temp/libxml2-2.7.8
  ./configure --host=${HOST} --prefix=${PREFIX}
  make ${JOBS}
  make install
  popd
fi

if [ "${build_pcre}" = "true"  ]; then
  rm -rf temp/pcre-8.30
  tar xzf files/pcre-8.30.tar.gz -C temp
  pushd temp/pcre-8.30
  ./configure --host=${HOST} --prefix=${PREFIX} --enable-utf8 \
    --enable-unicode-properties
  make ${JOBS}
  make install
  popd
fi

if [ "${build_gettext}" = "true"  ]; then
  rm -rf temp/gettext-0.18.1.1
  tar xzf files/gettext-0.18.1.1.tar.gz -C temp
  pushd temp/gettext-0.18.1.1
  ./configure --host=${HOST} --prefix=${PREFIX} --disable-nls
  make ${JOBS}
  make install
  popd
fi

if [ "${build_blas}" = "true"  ]; then
  rm -rf temp/BLAS
  tar xzf files/blas.tgz -C temp
  patch temp/BLAS/make.inc < patches/blas-make.patch
  pushd temp/BLAS
  make clean
  make
  cp blas.a ${PREFIX}/lib
  cp /usr/arm-linux-gnueabi/lib/libgfortran.so.3.0.0 \
    ${PREFIX}/lib/libgfortran.so.3
  mkdir tmp && cd tmp
  ar x ../blas.a
  cd ..
  ${HOST}-gfortran -shared -Wl,-soname=libblas.so -o libblas.so tmp/*.o
  cp libblas.so ${PREFIX}/lib
  rm -rf tmp
  popd
fi

if [ "${build_lapack}" = "true" ]; then
  rm -rf temp/lapack-3.4.1
  tar xzf files/lapack-3.4.1.tgz -C temp
  patch temp/lapack-3.4.1/make.inc < patches/lapack-make.patch
  pushd temp/lapack-3.4.1/SRC
  make clean
  make
  cd ..
  cp liblapack.a ${PREFIX}/lib
  mkdir tmp && cd tmp
  ar x ../liblapack.a
  cd ..
  ${HOST}-gfortran -shared -Wl,-soname=liblapack.so -o liblapack.so tmp/*.o
  cp liblapack.so ${PREFIX}/lib
  rm -rf tmp
  popd
fi

# Build disable but need to be fixed for scilab > 5.3.3
if [ "${build_hdf5}" = "true" ]; then
  rm -rf temp/hdf5-1.8.9
  # tar xzf files/hdf5-1.8.9.tar.gz -C temp
  # pushd temp/hdf5-1.8.9
  # ./configure --host=${HOST} --prefix=${PREFIX}
  # make ${JOBS}
  # make install
  # popd

  # That's some serious hack that we *need* to address at some point
  mkdir temp/hdf5-1.8.9
  pushd temp/hdf5-1.8.9

  wget http://ftp.fr.debian.org/debian/pool/main/h/hdf5/libhdf5-dev_1.8.8-9_armel.deb
  wget http://ftp.fr.debian.org/debian/pool/main/h/hdf5/libhdf5-7_1.8.8-9_armel.deb
  wget http://ftp.fr.debian.org/debian/pool/main/z/zlib/zlib1g_1.2.7.dfsg-13_armel.deb

  ar x libhdf5-dev_1.8.8-9_armel.deb
  tar xzf data.tar.gz
  rm data.tar.gz

  ar x libhdf5-7_1.8.8-9_armel.deb
  tar xzf data.tar.gz
  rm data.tar.gz

  cp -rpP usr/lib/* /data/local/usr/lib
  cp -rpP usr/include/* /data/local/usr/include

  ar x zlib1g_1.2.7.dfsg-13_armel.deb
  tar xzf data.tar.gz
  rm data.tar.gz
  cp -rpP lib/arm-linux-gnueabi/* /data/local/usr/lib

  popd
fi

exit 0
