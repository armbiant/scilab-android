/* -*- Mode: C; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <sys/mman.h>

#include <jni.h>

#include <linux/elf.h>

#include <android/log.h>
#include <android_native_app_glue.h>

#include "scilab_bootstrap.h"

#undef LOGI
#undef LOGW
#undef LOGE

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO,  "scilab-bootstrap", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN,  "scilab-bootstrap", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "scilab-bootstrap", __VA_ARGS__))
#define LOGF(...) ((void)__android_log_print(ANDROID_LOG_FATAL, "scilab-bootstrap", __VA_ARGS__))

struct engine {
  int dummy;
};

static struct android_app* app;
static const char** library_locations;
static void* apk_file;
static int apk_file_size;
static int (*scilab_main)(int, const char**);
static int scilab_main_argc;
static const char** scilab_main_argv;
static int sleep_time = 0;

/* Zip data structures */

/* compression methods */
#define STORE    0
#define DEFLATE  8
#define LZMA    14

struct local_file_header {
  uint32_t signature;
  uint16_t min_version;
  uint16_t general_flag;
  uint16_t compression;
  uint16_t lastmod_time;
  uint16_t lastmod_date;
  uint32_t crc32;
  uint32_t compressed_size;
  uint32_t uncompressed_size;
  uint16_t filename_size;
  uint16_t extra_field_size;
  char     data[0];
} __attribute__((__packed__));

struct cdir_entry {
  uint32_t signature;
  uint16_t creator_version;
  uint16_t min_version;
  uint16_t general_flag;
  uint16_t compression;
  uint16_t lastmod_time;
  uint16_t lastmod_date;
  uint32_t crc32;
  uint32_t compressed_size;
  uint32_t uncompressed_size;
  uint16_t filename_size;
  uint16_t extra_field_size;
  uint16_t file_comment_size;
  uint16_t disk_num;
  uint16_t internal_attr;
  uint32_t external_attr;
  uint32_t offset;
  char     data[0];
} __attribute__((__packed__));

#define CDIR_END_SIG 0x06054b50

struct cdir_end {
  uint32_t signature;
  uint16_t disk_num;
  uint16_t cdir_disk;
  uint16_t disk_entries;
  uint16_t cdir_entries;
  uint32_t cdir_size;
  uint32_t cdir_offset;
  uint16_t comment_size;
  char     comment[0];
} __attribute__((__packed__));

/* End of Zip data structures */

static void
engine_handle_cmd(struct android_app* app,
                  int32_t cmd)
{
  struct engine* engine = (struct engine*)app->userData;
  switch (cmd) {
    case APP_CMD_SAVE_STATE:
      break;
    case APP_CMD_INIT_WINDOW:
      break;
    case APP_CMD_TERM_WINDOW:
      break;
    case APP_CMD_GAINED_FOCUS:
      break;
    case APP_CMD_LOST_FOCUS:
      break;
  }
}

static char*
read_section(int fd,
             Elf32_Shdr* shdr)
{
  char* result;

  result = malloc(shdr->sh_size);
  if (lseek(fd, shdr->sh_offset, SEEK_SET) < 0) {
    close(fd);
    free(result);
    return NULL;
  }
  if (read(fd, result, shdr->sh_size) < shdr->sh_size) {
    close(fd);
    free(result);
    return NULL;
  }

  return result;
}

static void
free_ptrarray(void** pa)
{
  void** rover = pa;

  while (*rover != NULL)
    free(*rover++);

  free(pa);
}

jobjectArray
Java_org_scilab_Bootstrap_dlneeds(JNIEnv* env,
                                  jobject clazz,
                                  jstring library)
{
  char** needed;
  int n_needed;
  const jbyte* libName;
  jclass String;
  jobjectArray result;

  libName = (*env)->GetStringUTFChars(env, library, NULL);

  needed = scilab_dlneeds(libName);

  (*env)->ReleaseStringUTFChars(env, library, libName);

  if (needed == NULL)
    return NULL;

  n_needed = 0;
  while (needed[n_needed] != NULL)
    n_needed++;

  /* Allocate return value */

  String = (*env)->FindClass(env, "java/lang/String");
  if (String == NULL) {
    LOGE("Could not find the String class");
    free_ptrarray((void **) needed);
    return NULL;
  }

  result = (*env)->NewObjectArray(env, n_needed, String, NULL);
  if (result == NULL) {
    LOGE("Could not create the String array");
    free_ptrarray((void **) needed);
    return NULL;
  }

  for (n_needed = 0; needed[n_needed] != NULL; n_needed++)
    (*env)->SetObjectArrayElement(env, result, n_needed, (*env)->NewStringUTF(env, needed[n_needed]));

  free_ptrarray((void **) needed);

  return result;
}

jint
Java_org_scilab_Bootstrap_dlopen(JNIEnv* env,
                                 jobject clazz,
                                 jstring library)
{
  const jbyte* libName = (*env)->GetStringUTFChars(env, library, NULL);
  void* p = scilab_dlopen(libName);

  (*env)->ReleaseStringUTFChars(env, library, libName);

  return (jint) p;
}

jint
Java_org_scilab_Bootstrap_dlsym(JNIEnv* env,
                                jobject clazz,
                                jint handle,
                                jstring symbol)
{
  const jbyte* symName = (*env)->GetStringUTFChars(env, symbol, NULL);
  void* p = scilab_dlsym((void*) handle, symName);

  (*env)->ReleaseStringUTFChars(env, symbol, symName);

  return (jint) p;
}

jint
Java_org_scilab_Bootstrap_dlcall(JNIEnv* env,
                                 jobject clazz,
                                 jint function,
                                 jobject argument)
{
  jclass StringArray = (*env)->FindClass(env, "[Ljava/lang/String;");

  if (StringArray == NULL) {
    LOGE("Could not find String[] class");
    return 0;
  }

  if ((*env)->IsInstanceOf(env, argument, StringArray)) {
    int argc = (*env)->GetArrayLength(env, argument);
    const char** argv = malloc(sizeof(char*) * (argc+1));
    int i, result;
    for (i = 0; i < argc; i++) {
      argv[i] = (*env)->GetStringUTFChars(env, (*env)->GetObjectArrayElement(env, argument, i), NULL);
    }
    argv[argc] = NULL;

    result = scilab_dlcall_argc_argv((void*) function, argc, argv);

    for (i = 0; i < argc; i++)
      (*env)->ReleaseStringUTFChars(env, (*env)->GetObjectArrayElement(env, argument, i), argv[i]);

    free(argv);
    return result;
  }

  return 0;
}

// public static native boolean setup(String dataDir,
//                                    String apkFile,
//                                    String[] ld_library_path);

jboolean Java_org_scilab_Bootstrap_setup__Ljava_lang_String_2Ljava_lang_String_2_3Ljava_lang_String_2
(JNIEnv* env,
 jobject clazz,
 jstring dataDir,
 jstring apkFile,
 jobjectArray ld_library_path)
{
  struct stat st;
  int i, n, fd;
  const jbyte* dataDirPath;
  const jbyte* apkFilePath;
  char* lib_dir;

  n = (*env)->GetArrayLength(env, ld_library_path);

  library_locations = malloc((n+2) * sizeof(char *));

  dataDirPath = (*env)->GetStringUTFChars(env, dataDir, NULL);

  lib_dir = malloc(strlen(dataDirPath) + 5);
  strcpy(lib_dir, dataDirPath);
  strcat(lib_dir, "/lib");

  (*env)->ReleaseStringUTFChars(env, dataDir, dataDirPath);

  library_locations[0] = lib_dir;

  for (i = 0; i < n; i++) {
    const jbyte* s = (*env)->GetStringUTFChars(env, (*env)->GetObjectArrayElement(env, ld_library_path, i), NULL);
    library_locations[i+1] = strdup(s);
    (*env)->ReleaseStringUTFChars(env, (*env)->GetObjectArrayElement(env, ld_library_path, i), s);
  }

  library_locations[n+1] = NULL;

  for (n = 0; library_locations[n] != NULL; n++)
    LOGI("library_locations[%d] = %s", n, library_locations[n]);

  apkFilePath =  (*env)->GetStringUTFChars(env, apkFile, NULL);

  fd = open(apkFilePath, O_RDONLY);
  if (fd == -1) {
    LOGE("Could not open %s", apkFilePath);
    (*env)->ReleaseStringUTFChars(env, apkFile, apkFilePath);
    return JNI_FALSE;
  }
  if (fstat(fd, &st) == -1) {
    LOGE("Could not fstat %s", apkFilePath);
    close(fd);
    (*env)->ReleaseStringUTFChars(env, apkFile, apkFilePath);
    return JNI_FALSE;
  }
  apk_file = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
  close(fd);

  if (apk_file == MAP_FAILED) {
    LOGE("Could not mmap %s", apkFilePath);
    (*env)->ReleaseStringUTFChars(env, apkFile, apkFilePath);
    return JNI_FALSE;
  }
  apk_file_size = st.st_size;

  (*env)->ReleaseStringUTFChars(env, apkFile, apkFilePath);

  return JNI_TRUE;
}

// public static native boolean setup(int scilab_main_ptr,
//                                    Object scilab_main_argument,
//                                    int scilab_main_delay);

jboolean
Java_org_scilab_Bootstrap_setup__ILjava_lang_Object_2I(JNIEnv* env,
                                                       jobject clazz,
                                                       void* scilab_main_ptr,
                                                       jobject scilab_main_argument,
                                                       jint scilab_main_delay)
{
  jclass StringArray;
  int i;

  scilab_main = scilab_main_ptr;

  StringArray = (*env)->FindClass(env, "[Ljava/lang/String;");
  if (StringArray == NULL) {
    LOGE("Could not find String[] class");
    return JNI_FALSE;
  }

  if (!(*env)->IsInstanceOf(env, scilab_main_argument, StringArray)) {
    LOGE("scilab_main_argument is not a String[]?");
    return JNI_FALSE;
  }

  scilab_main_argc = (*env)->GetArrayLength(env, scilab_main_argument);
  scilab_main_argv = malloc(sizeof(char *) * (scilab_main_argc+1));

  for (i = 0; i < scilab_main_argc; i++) {
    const jbyte* s = (*env)->GetStringUTFChars(env, (*env)->GetObjectArrayElement(env, scilab_main_argument, i), NULL);
    scilab_main_argv[i] = strdup(s);
    (*env)->ReleaseStringUTFChars(env, (*env)->GetObjectArrayElement(env, scilab_main_argument, i), s);
  }
  scilab_main_argv[scilab_main_argc] = NULL;

  sleep_time = scilab_main_delay;

  return JNI_TRUE;
}

// public static native int getpid();

jint
Java_org_scilab_Bootstrap_getpid(JNIEnv* env,
                                 jobject clazz)
{
  return getpid();
}


// public static native void system(String cmdline);

jint
Java_org_scilab_Bootstrap_system(JNIEnv* env,
                                 jobject clazz,
                                 jstring cmdline)
{
  const jbyte* s = (*env)->GetStringUTFChars(env, cmdline, NULL);

  LOGI("system(%s)", s);

  system(s);

  (*env)->ReleaseStringUTFChars(env, cmdline, s);
}

// public static native void putenv(String string);

void
Java_org_scilab_Bootstrap_putenv(JNIEnv* env,
                                 jobject clazz,
                                 jstring string)
{
  const jbyte* s = (*env)->GetStringUTFChars(env, string, NULL);

  LOGI("putenv(%s)", s);

  putenv(s);

  (*env)->ReleaseStringUTFChars(env, string, s);
}

void
Java_org_scilab_Bootstrap_setEnvironment(JNIEnv* env,
                                         jobject this,
                                         jstring _key,
                                         jstring _value)
{
  const char* key;
  const char* value;

  key = (*env)->GetStringUTFChars(env, _key, 0);
  value = (*env)->GetStringUTFChars(env, _value, 0);

  /*
   * The variable should not exist but we use the replace flag just in case.
   */
  setenv(key, value, 1);

  (*env)->ReleaseStringUTFChars(env, _key, key);
  (*env)->ReleaseStringUTFChars(env, _value, value);
}

char**
scilab_dlneeds(const char* library)
{
  int i, fd, n_needed;
  char** result;
  char* shstrtab;
  char* dynstr;
  Elf32_Ehdr hdr;
  Elf32_Shdr shdr;
  Elf32_Dyn dyn;

  /* Open library and read ELF header */

  fd = open(library, O_RDONLY);

  if (fd == -1) {
    LOGE("scilab_dlneeds: Could not open library %s: %s", library, strerror(errno));
    return NULL;
  }

  if (read(fd, &hdr, sizeof(hdr)) < sizeof(hdr)) {
    LOGE("scilab_dlneeds: Could not read ELF header of %s", library);
    close(fd);
    return NULL;
  }

  /* Read in .shstrtab */

  if (lseek(fd, hdr.e_shoff + hdr.e_shstrndx * sizeof(shdr), SEEK_SET) < 0) {
    LOGE("scilab_dlneeds: Could not seek to .shstrtab section header of %s", library);
    close(fd);
    return NULL;
  }
  if (read(fd, &shdr, sizeof(shdr)) < sizeof(shdr)) {
    LOGE("scilab_dlneeds: Could not read section header of %s", library);
    close(fd);
    return NULL;
  }

  shstrtab = read_section(fd, &shdr);
  if (shstrtab == NULL)
    return NULL;

  /* Read section headers, looking for .dynstr section */

  if (lseek(fd, hdr.e_shoff, SEEK_SET) < 0) {
    LOGE("scilab_dlneeds: Could not seek to section headers of %s", library);
    close(fd);
    return NULL;
  }
  for (i = 0; i < hdr.e_shnum; i++) {
    if (read(fd, &shdr, sizeof(shdr)) < sizeof(shdr)) {
      LOGE("scilab_dlneeds: Could not read section header of %s", library);
      close(fd);
      return NULL;
    }
    if (shdr.sh_type == SHT_STRTAB &&
        strcmp(shstrtab + shdr.sh_name, ".dynstr") == 0) {
      dynstr = read_section(fd, &shdr);
      if (dynstr == NULL) {
        free(shstrtab);
        return NULL;
      }
      break;
    }
  }

  if (i == hdr.e_shnum) {
    LOGE("scilab_dlneeds: No .dynstr section in %s", library);
    close(fd);
    return NULL;
  }

  /* Read section headers, looking for .dynamic section */

  if (lseek(fd, hdr.e_shoff, SEEK_SET) < 0) {
    LOGE("scilab_dlneeds: Could not seek to section headers of %s", library);
    close(fd);
    return NULL;
  }
  for (i = 0; i < hdr.e_shnum; i++) {
    if (read(fd, &shdr, sizeof(shdr)) < sizeof(shdr)) {
      LOGE("scilab_dlneeds: Could not read section header of %s", library);
      close(fd);
      return NULL;
    }
    if (shdr.sh_type == SHT_DYNAMIC) {
      int dynoff;
      int* libnames;

      /* Count number of DT_NEEDED entries */
      n_needed = 0;
      if (lseek(fd, shdr.sh_offset, SEEK_SET) < 0) {
        LOGE("scilab_dlneeds: Could not seek to .dynamic section of %s", library);
        close(fd);
        return NULL;
      }
      for (dynoff = 0; dynoff < shdr.sh_size; dynoff += sizeof(dyn)) {
        if (read(fd, &dyn, sizeof(dyn)) < sizeof(dyn)) {
          LOGE("scilab_dlneeds: Could not read .dynamic entry of %s", library);
          close(fd);
          return NULL;
        }
        if (dyn.d_tag == DT_NEEDED)
          n_needed++;
      }


      result = malloc((n_needed+1) * sizeof(char *));

      n_needed = 0;
      if (lseek(fd, shdr.sh_offset, SEEK_SET) < 0) {
        LOGE("scilab_dlneeds: Could not seek to .dynamic section of %s", library);
        close(fd);
        free(result);
        return NULL;
      }
      for (dynoff = 0; dynoff < shdr.sh_size; dynoff += sizeof(dyn)) {
        if (read(fd, &dyn, sizeof(dyn)) < sizeof(dyn)) {
          LOGE("scilab_dlneeds: Could not read .dynamic entry in %s", library);
          close(fd);
          free(result);
          return NULL;
        }
        if (dyn.d_tag == DT_NEEDED) {
          result[n_needed] = strdup(dynstr + dyn.d_un.d_val);
          n_needed++;
        }
      }

      close(fd);
      free(dynstr);
      free(shstrtab);
      result[n_needed] = NULL;
      return result;
    }
  }

  LOGE("scilab_dlneeds: Could not find .dynamic section in %s", library);
  close(fd);
  return NULL;
}

void*
scilab_dlopen(const char* library)
{
  /*
   * We should *not* try to just dlopen() the bare library name
   * first, as the stupid dynamic linker remembers for each library
   * basename if loading it has failed. Thus if you try loading it
   * once, and it fails because of missing needed libraries, and
   * your load those, and then try again, it fails with an
   * infuriating message "failed to load previously" in the log.
   *
   * We *must* first dlopen() all needed libraries, recursively. It
   * shouldn't matter if we dlopen() a library that already is
   * loaded, dlopen() just returns the same value then.
   */

  typedef struct loadedLib {
    const char* name;
    void* handle;
    struct loadedLib *next;
  } *loadedLib;
  static loadedLib loaded_libraries = NULL;

  loadedLib rover;
  loadedLib new_loaded_lib;

  struct stat st;
  void* p;
  char* full_name;
  char** needed;
  int i;
  int found;

  rover = loaded_libraries;
  while (rover != NULL &&
      strcmp(rover->name, library) != 0)
    rover = rover->next;

  if (rover != NULL)
    return rover->handle;

  found = 0;
  if (library[0] == '/') {
    full_name = strdup(library);

    if (stat(full_name, &st) == 0 &&
        S_ISREG(st.st_mode))
      found = 1;
    else
      free(full_name);
  } else {
    for (i = 0; !found && library_locations[i] != NULL; i++) {
      full_name = malloc(strlen(library_locations[i]) + 1 + strlen(library) + 1);
      strcpy(full_name, library_locations[i]);
      strcat(full_name, "/");
      strcat(full_name, library);

      if (stat(full_name, &st) == 0 &&
          S_ISREG(st.st_mode))
        found = 1;
      else
        free(full_name);
    }
  }

  if (!found) {
    LOGE("scilab_dlopen: Library %s not found", library);
    return NULL;
  }

  needed = scilab_dlneeds(full_name);
  if (needed == NULL) {
    free(full_name);
    return NULL;
  }

  for (i = 0; needed[i] != NULL; i++) {
    if (scilab_dlopen(needed[i]) == NULL) {
      free_ptrarray((void **) needed);
      free(full_name);
      return NULL;
    }
  }
  free_ptrarray((void **) needed);

  p = dlopen(full_name, RTLD_LOCAL);
  LOGI("dlopen(%s) = %p", full_name, p);
  free(full_name);
  if (p == NULL)
    LOGE("scilab_dlopen: Error from dlopen(%s): %s", library, dlerror());

  new_loaded_lib = malloc(sizeof(*new_loaded_lib));
  new_loaded_lib->name = strdup(library);
  new_loaded_lib->handle = p;

  new_loaded_lib->next = loaded_libraries;
  loaded_libraries = new_loaded_lib;

  return p;
}

void*
scilab_dlsym(void* handle,
             const char* symbol)
{
  void* p = dlsym(handle, symbol);
  if (p == NULL)
    LOGE("scilab_dlsym: %s", dlerror());
  return p;
}

int
scilab_dladdr(void* addr,
              Dl_info* info)
{
  FILE* maps;
  char line[200];
  int result;
  int found;

  result = dladdr(addr, info);
  if (result == 0) {
    return 0;
  }

  maps = fopen("/proc/self/maps", "r");
  if (maps == NULL) {
    LOGE("scilab_dladdr: Could not open /proc/self/maps: %s", strerror(errno));
    return 0;
  }

  found = 0;
  while (fgets(line, sizeof(line), maps) != NULL &&
      line[strlen(line)-1] == '\n') {
    void* lo;
    void* hi;
    char file[sizeof(line)];
    file[0] = '\0';
    if (sscanf(line, "%x-%x %*s %*x %*x:%*x %*d %[^\n]", &lo, &hi, file) == 3) {
      if (addr >= lo && addr < hi) {
        if (info->dli_fbase != lo) {
          LOGE("scilab_dladdr: Base for %s in /proc/self/maps %p doesn't match what dladdr() said", file, lo);
          fclose(maps);
          return 0;
        }
        info->dli_fname = strdup(file);
        found = 1;
        break;
      }
    }
  }
  if (!found)
    LOGE("scilab_dladdr: Did not find %p in /proc/self/maps", addr);
  fclose(maps);

  return result;
}

static uint32_t cdir_entry_size(struct cdir_entry* entry)
{
  return sizeof(*entry) +
    letoh16(entry->filename_size) +
    letoh16(entry->extra_field_size) +
    letoh16(entry->file_comment_size);
}

static struct cdir_entry*
find_cdir_entry(struct cdir_entry* entry, int count, const char* name)
{
  size_t name_size = strlen(name);
  while (count--) {
    if (letoh16(entry->filename_size) == name_size &&
        !memcmp(entry->data, name, name_size))
      return entry;
    entry = (struct cdir_entry *)((char *)entry + cdir_entry_size(entry));
  }
  return NULL;
}

void*
scilab_apkentry(const char* filename,
                size_t* size)
{
  struct cdir_end* dirend = (struct cdir_end*)((char*) apk_file + apk_file_size - sizeof(*dirend));
  uint32_t cdir_offset;
  uint16_t cdir_entries;
  struct cdir_entry* cdir_start;
  struct cdir_entry* entry;
  struct local_file_header* file;
  void* data;

  while ((void*) dirend > apk_file &&
      letoh32(dirend->signature) != CDIR_END_SIG)
    dirend = (struct cdir_end*)((char*)dirend - 1);
  if (letoh32(dirend->signature) != CDIR_END_SIG) {
    LOGE("scilab_apkentry: Could not find end of central directory record");
    return;
  }

  cdir_offset = letoh32(dirend->cdir_offset);
  cdir_entries = letoh16(dirend->cdir_entries);
  cdir_start = (struct cdir_entry*)((char*)apk_file + cdir_offset);

  if (*filename == '/')
    filename++;

  entry = find_cdir_entry(cdir_start, cdir_entries, filename);

  if (entry == NULL) {
    LOGE("scilab_apkentry: Could not find %s", filename);
    return NULL;
  }
  file = (struct local_file_header*)((char*) apk_file + letoh32(entry->offset));

  if (letoh16(file->compression) != STORE) {
    LOGE("scilab_apkentry: File %s is compressed", filename);
    return NULL;
  }

  data = ((char*)&file->data) + letoh16(file->filename_size) + letoh16(file->extra_field_size);
  *size = file->uncompressed_size;

  return data;
}

int
scilab_dlcall_argc_argv(void* function,
                        int argc,
                        const char** argv)
{
  int (*fp)(int, const char**) = function;
  int result = fp(argc, argv);

  return result;
}

JavaVM*
scilab_get_javavm(void)
{
  return app->activity->vm;
}

void
android_main(struct android_app* state)
{
  struct engine engine;
  Dl_info scilab_main_info;

  app = state;

  memset(&engine, 0, sizeof(engine));
  state->userData = &engine;
  state->onAppCmd = engine_handle_cmd;

  if (scilab_dladdr(scilab_main, &scilab_main_info) != 0) {
    scilab_main_argv[0] = scilab_main_info.dli_fname;
  }

  if (sleep_time != 0)
    sleep(sleep_time);

  scilab_main(scilab_main_argc, scilab_main_argv);

  exit(0);
}

/* vim:set shiftwidth=2 softtabstop=2 expandtab: */
